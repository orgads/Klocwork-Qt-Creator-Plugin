/****************************************************************************
**
** Copyright (C) 2015 Emenda Nordic AB
** Author: Jacob Lärfors (jacob.larfors@emenda.eu)
**
** GNU Lesser General Public License Usage
** This file may be used under the terms of the GNU Lesser
** General Public License version 2.1 or version 3 as published by the Free
** Software Foundation and appearing in the file LICENSE.LGPLv21 and
** LICENSE.LGPLv3 included in the packaging of this file.  Please review the
** following information to ensure the GNU Lesser General Public License
** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
****************************************************************************/

#include "klocworkprojectsettings.h"
#include "klocworkpluginconstants.h"

#include <projectexplorer/project.h>

using namespace ProjectExplorer;

using namespace Klocwork::Constants;
using namespace Klocwork::Internal;

KlocworkProjectSettings::KlocworkProjectSettings(Project *project)
    : QWidget(), m_project(project) {

    m_ui.setupUi(this);

    m_ui.klocworkProjectUrlEdit->setText(
        m_project->namedSettings(tr(KW_SETTINGS_PROJECT_URL)).toString());
    m_ui.klocworkBuildSpecEdit->setText(
        m_project->namedSettings(tr(KW_SETTINGS_BUILD_SPEC)).toString());

    connect(m_ui.klocworkProjectUrlEdit, SIGNAL(editingFinished()), this,
            SLOT(saveKlocworkProjectURL()));

    connect(m_ui.klocworkBuildSpecEdit, SIGNAL(editingFinished()), this,
            SLOT(saveKlocworkBuildSpec()));
}

void KlocworkProjectSettings::saveKlocworkProjectURL() {
    m_ui.klocworkProjectUrlEdit->setText(m_ui.klocworkProjectUrlEdit->text().trimmed());
    m_project->setNamedSettings(tr(KW_SETTINGS_PROJECT_URL),
                                QVariant(m_ui.klocworkProjectUrlEdit->text()));
}

void KlocworkProjectSettings::saveKlocworkBuildSpec() {
    m_ui.klocworkBuildSpecEdit->setText(m_ui.klocworkBuildSpecEdit->text().trimmed());
    m_project->setNamedSettings(tr(KW_SETTINGS_BUILD_SPEC),
                                QVariant(m_ui.klocworkBuildSpecEdit->text()));
}
