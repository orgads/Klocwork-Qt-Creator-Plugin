/****************************************************************************
**
** Copyright (C) 2015 Emenda Nordic AB
** Author: Jacob Lärfors (jacob.larfors@emenda.eu)
**
** GNU Lesser General Public License Usage
** This file may be used under the terms of the GNU Lesser
** General Public License version 2.1 or version 3 as published by the Free
** Software Foundation and appearing in the file LICENSE.LGPLv21 and
** LICENSE.LGPLv3 included in the packaging of this file.  Please review the
** following information to ensure the GNU Lesser General Public License
** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
****************************************************************************/

#include "klocworkproject.h"
#include "klocworkpluginconstants.h"

#include <coreplugin/id.h>

#include <projectexplorer/buildconfiguration.h>
#include <projectexplorer/project.h>
#include <projectexplorer/target.h>

#include <QObject>

using namespace Klocwork::Internal;
using namespace ProjectExplorer;

KlocworkProject::KlocworkProject(Project *project) : m_project(project) {
    m_klocworkProjectDir = project->activeTarget()
                               ->activeBuildConfiguration()
                               ->buildDirectory()
                               .appendPath(QObject::tr(".kwlp"))
                               .toString();
    m_klocworkSettingsDir = project->activeTarget()
                                ->activeBuildConfiguration()
                                ->buildDirectory()
                                .appendPath(QObject::tr(".kwps"))
                                .toString();
    m_klocworkReportsFile = project->activeTarget()
                                ->activeBuildConfiguration()
                                ->buildDirectory()
                                .appendPath(QObject::tr("klocworkResults.csv"))
                                .toString();

    m_klocworkServerProjectURL =
        project->namedSettings(QObject::tr(Constants::KW_SETTINGS_PROJECT_URL))
            .toString();
    m_klocworkBuildSpec =
        project->namedSettings(QObject::tr(Constants::KW_SETTINGS_BUILD_SPEC))
            .toString();
}

bool KlocworkProject::updateSettings() {
    bool returnValue = false;
    m_klocworkBuildSpec =
        m_project->namedSettings(QObject::tr(Constants::KW_SETTINGS_BUILD_SPEC))
            .toString();
    
    if (m_klocworkBuildSpec.isEmpty()) {
        m_klocworkBuildSpec = Constants::EMPTY_BUILDSPEC;
    }

    if (m_klocworkServerProjectURL !=
        m_project->namedSettings(QObject::tr(Constants::KW_SETTINGS_PROJECT_URL))
            .toString()) {
        m_klocworkServerProjectURL =
            m_project->namedSettings(
                           QObject::tr(Constants::KW_SETTINGS_PROJECT_URL))
                .toString();
        returnValue = true;
    }

    m_klocworkProjectDir = m_project->activeTarget()
                               ->activeBuildConfiguration()
                               ->buildDirectory()
                               .appendPath(QObject::tr(".kwlp"))
                               .toString();
    m_klocworkSettingsDir = m_project->activeTarget()
                                ->activeBuildConfiguration()
                                ->buildDirectory()
                                .appendPath(QObject::tr(".kwps"))
                                .toString();
    m_klocworkReportsFile = m_project->activeTarget()
                                ->activeBuildConfiguration()
                                ->buildDirectory()
                                .appendPath(QObject::tr("klocworkResults.csv"))
                                .toString();

    return returnValue;
}

Project *KlocworkProject::getProject() { return m_project; }

const Core::Id KlocworkProject::getProjectId() const { return m_project->id(); }

const QString KlocworkProject::getKlocworkServerProjectURL() const {
    return m_klocworkServerProjectURL;
}

const QString KlocworkProject::getKlocworkBuildSpec() const {
    return m_klocworkBuildSpec;
}

const QString &KlocworkProject::getKlocworkProjectDir() const {
    return m_klocworkProjectDir;
}

const QString &KlocworkProject::getKlocworkSettingsDir() const {
    return m_klocworkSettingsDir;
}

const QString &KlocworkProject::getKlocworkReportsFile() const {
    return m_klocworkReportsFile;
}
