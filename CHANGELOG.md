# Revision History

## v1.2
  * SSL connections now automatically work when given an https scheme
  * Improved error handling

## v1.1
  * CMake projects no longer crash when loading them with the plugin enabled
  * Klocwork initialisation no longer enters infinite loop when passed a bad Klocwork project URL
  * Updated documentation to reflect how to set the plugin for different versions of Qt Creator
  * Plugin now checks that the latest build spec, license settings, and project URL are always used before an analysis
  * Better error handling when providing garbage data to Klocwork settings
  * The Klocwork Project Settings page now allows you to connect to a project on the Klocwork Server via project URL
  * Minor GUI improvements
  * Klocwork projects now show previously analyzed defects immediately when re-opening Qt Creator
  * Improved error handling for missing results file

## v1.0
  * Initial Release
